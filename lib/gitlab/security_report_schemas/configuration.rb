# frozen_string_literal: true

module Gitlab
  module SecurityReportSchemas
    # Holds the configuration of the gem
    class Configuration
      OPTIONS = {
        schemas_path: -> { SecurityReportSchemas.root_path.join("schemas") },
        deprecated_versions: -> { [] }
      }.freeze

      OPTIONS.each do |option, default_value|
        define_method(option) do
          instance_variable_get("@#{option}") || default_value.call
        end

        attr_writer option
      end
    end
  end
end
